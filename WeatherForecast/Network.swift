

import UIKit
import Foundation
import Alamofire

final class APIClient {
    
    static let shared = APIClient()
    

    private let baseURL = URL(string: "http://api.openweathermap.org/data/2.5/forecast/")!
    
    func send<T:Codable>(apiRequest:APIRequest, completionHandler:((_ data:T?,_ success:Bool)->())?)
    {
        let url=self.baseURL.appendingPathComponent(apiRequest.path)
        let params = apiRequest.parameters
        
        Alamofire.request(url,
                          method: .get,
                          parameters: params,
                          encoding: URLEncoding.default)
            .responseJSON(completionHandler: { (response) in
                if let json = response.result.value as? [String:Any]
                {
                    do {
                            let itemData : Data? = try? JSONSerialization.data(withJSONObject: json
                                , options: .prettyPrinted)
                            let model: T = try JSONDecoder().decode(T.self, from: itemData ?? Data())
                            if let complete = completionHandler
                            {
                                complete(model,true)
                            }
                        } catch let error
                        {
                            if let complete = completionHandler
                            {
                                complete(nil,false)
                            }
                            print("Network error: \(error)")
                        }
                }
            })
    }
}

protocol APIRequest
{
    var path: String { get }
    var parameters: [String : Any] { get }
}

