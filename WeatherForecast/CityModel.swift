//
//  CityModel.swift
//  WeatherForecast
//
//  Created by ersun alkan on 16.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit

struct CityModel:Codable
{
    let id : Int?
    let name : String?
    let country : String?
    
    enum CodingKeys: String, CodingKey
    {
        case id = "id"
        case name = "name"
        case country = "country"
    }
}
