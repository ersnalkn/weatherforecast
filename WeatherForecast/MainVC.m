//
//  MainVC.m
//  WeatherForecast
//
//  Created by Seda Deliorman on 30/06/16.
//  Copyright © 2016 N11.com. All rights reserved.
//

#import "MainVC.h"
#import "WeatherForecast-Swift.h"


@interface MainVC ()

@end

@implementation MainVC

NSArray* cityList;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupUI];
    
    [self mainRequest];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(mainRequest)
                                                 name:@"CityListNeedsRefresh"
                                               object:nil];
}

-(void)setupUI
{
    UIBarButtonItem* addButton = [[UIBarButtonItem alloc]
                                initWithTitle:@"Ekle"
                                style:UIBarButtonItemStyleDone
                                target:self
                                action:@selector(navigateAddNewCity)];
    self.navigationItem.rightBarButtonItem=addButton;
    
    self.navigationItem.title=@"Hava Durumu";
}

-(void)mainRequest
{
    cityList = [[LocalData new] getCityList];
    [self displayData];
}

-(void)displayData
{
    if (cityList.count>0)
    {
        [self.tableView reloadData];
    }
    else
    {
        [self navigateAddNewCity];
    }
}


#pragma mark navigation

-(void)navigateAddNewCity
{
    AddCityViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddCityViewController"];
    [self.navigationController pushViewController:vc animated:true];
}

-(void)navigateWeatherForecastWith:(SavedCityModel*)city
{
    CityWeatherViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"CityWeatherViewController"];
    vc.selectedCity=city;
    [self.navigationController pushViewController:vc animated:true];
}


#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cityList count];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    SavedCityModel* city=cityList[indexPath.row];
    cell.textLabel.text=city.cityName;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SavedCityModel* city=cityList[indexPath.row];
    [self navigateWeatherForecastWith:city];
}


#pragma mark dealloc

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
