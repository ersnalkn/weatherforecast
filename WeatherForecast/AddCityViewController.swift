//
//  CitySearchViewController.swift
//  WeatherForecast
//
//  Created by ersun alkan on 16.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

class AddCityViewController: UIViewController {

    @IBOutlet weak var cityNameField: UITextField!
    
    @IBOutlet weak var dayCountButton: UIButton!
    
    var cityName:String?
    
    var numbersOfDay:Int=1{
        willSet
        {
            dayCountButton.setTitle("\(newValue)", for: .normal)
        }
    }
    
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.setupUI()
       
    }
    
    func setupUI()
    {
        let saveButton=UIBarButtonItem(title: "Kaydet",
                                       style: .done,
                                       target: self,
                                       action: Selector(("saveButtonPressed")))
        self.navigationItem.rightBarButtonItem=saveButton
    }
    
    @objc func saveButtonPressed()
    {
        self.view.endEditing(true)
        self.cityName=self.cityNameField.text
        guard let cityName = self.cityName
            , cityName.count > 0 else
        {
            //show error
            return            
        }
        
        let city=SavedCityModel(with: cityName, day: self.numbersOfDay)
        LocalData().addCity(city: city)
        NotificationCenter.default.post(name: NotificationConstants.CityListNeedsRefresh.notification, object: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    

    @IBAction func dayCountButtonAction(_ sender: Any)
    {
        var numberOfDay = [Int]()
        numberOfDay += 1...16
        ActionSheetStringPicker(title: "Date",
                                rows: numberOfDay ,
                                initialSelection: 0,
                                doneBlock: {[weak self] (picker, indexes, values) in
                                    self?.numbersOfDay=values as! Int
            },cancel: nil,
              origin: self.view).show()
    }
}
