//
//  CityWeatherViewController.swift
//  WeatherForecast
//
//  Created by ersun alkan on 16.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit

@objc class CityWeatherViewController: UIViewController
{

    @IBOutlet weak var tableView:UITableView!
    
    @objc var selectedCity:SavedCityModel!
    
    var responseWeather:CityWeatherModel?
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.mainRequest()
        
        self.setupUI()
        
    }
    
    func setupUI()
    {
        self.navigationItem.title=selectedCity.cityName
    }

    func mainRequest()
    {
        let request = CityWeatherRequest(q: selectedCity.cityName, cnt: selectedCity.numbersOfDay)
        APIClient.shared.send(apiRequest: request) {[weak self] (data:CityWeatherModel?,success:Bool) in
            if success,let data = data
            {
                self?.responseWeather=data
                self?.displayData()
            }
        }
    }
    
    func displayData()
    {
        self.tableView.reloadData()
    }
    
}


extension CityWeatherViewController:UITableViewDelegate
{
    
}

extension CityWeatherViewController:UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return responseWeather?.forecastList.count ?? 0 // extenstion
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell:WeatherCell = tableView.dequeueReusableCell(with: indexPath)
        let model:DailyForecastModel = responseWeather!.forecastList[indexPath.row]
        cell.setModel(model: model)
        return cell
    }
}
