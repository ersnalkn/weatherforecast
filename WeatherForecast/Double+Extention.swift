//
//  String+Extention.swift
//  WeatherForecast
//
//  Created by ersun alkan on 16.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit

extension Double {
    
    
    func toCelsius() -> String {
        return "\(Int(self)) °C"
    }
    
}
