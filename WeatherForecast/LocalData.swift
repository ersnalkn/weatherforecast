//
//  LocalData.swift
//  WeatherForecast
//
//  Created by ersun alkan on 16.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit
import Foundation



@objc class LocalData:NSObject
{
    
    let userDefaults = UserDefaults.standard

    func addCity(city:SavedCityModel)
    {
        var list=[city]
        if let cityList = loadDataArray()
        {
            list.append(contentsOf: cityList)
        }
        let encodedData: Data = archiveDataArray(dataArray: list)
        userDefaults.set(encodedData, forKey: Constants.CityList.rawValue)
    }
    
    
    @objc func getCityList()-> NSArray?
    {
        if let array = loadDataArray()
        {
            return NSArray(array: array)
        }
        return nil
    }
    
    
    private func archiveDataArray(dataArray : [SavedCityModel]) -> Data
    {
        do {
            let data = try NSKeyedArchiver.archivedData(withRootObject: dataArray, requiringSecureCoding: false)
            return data
        }
        catch
        {
            fatalError("archiveDataArray - encode error: \(error)")
        }
    }
    
    private func loadDataArray() -> [SavedCityModel]?
    {
        guard let unarchivedObject = UserDefaults.standard.data(forKey: Constants.CityList.rawValue)
            else
        {
            return nil
        }
        do
        {
            guard let array = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(unarchivedObject) as? [SavedCityModel] else
            {
                fatalError("loadWidgetDataArray - empty array")
            }
            return array
        } catch
        {
            fatalError("loadDataArray - encode error: \(error)")
        }
    }

}
