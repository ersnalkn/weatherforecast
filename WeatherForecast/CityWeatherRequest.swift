//
//  CityWeatherRequest.swift
//  WeatherForecast
//
//  Created by ersun alkan on 16.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit

class CityWeatherRequest: APIRequest
{
    var path = "daily"
    
    var parameters = [String: Any]()
    
    init(q:String,cnt:Int=1)
    {
        parameters["q"] = q
        parameters["cnt"] = cnt
        parameters["APPID"] = "41a47c0c4d7d86694bd472f51e33f937"
        parameters["units"] = "metric"
        
    }
}
