//
//  CityWeatherModel.swift
//  WeatherForecast
//
//  Created by ersun alkan on 16.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit

struct CityWeatherModel: Codable
{
    let city : CityModel?
    let cnt : Int?
    let forecastList : [DailyForecastModel]
    
    enum CodingKeys: String, CodingKey
    {
        case city = "city"
        case cnt = "cnt"
        case forecastList = "list"
    }
}


struct DailyForecastModel:Codable
{
    let dt : Int
    let temp : TemperatureModel
    let weather : [WeatherModel]
    
    enum CodingKeys: String, CodingKey
    {
        case dt = "dt"
        case temp = "temp"
        case weather = "weather"
    }
}

struct TemperatureModel:Codable
{
    let dayTemperature : Double
    let minTemperature : Double
    let maxTemperature : Double
    let nightTemperature : Double
    
    enum CodingKeys: String, CodingKey
    {
        case dayTemperature = "day"
        case minTemperature = "min"
        case maxTemperature = "max"
        case nightTemperature = "night"
    }
    
}

struct WeatherModel:Codable
{
    let id : Int?
    let main : String?
    let description : String?
    
    enum CodingKeys: String, CodingKey
    {
        case id = "id"
        case main = "main"
        case description = "description"
    }
}
