//
//  WeatherCell.swift
//  WeatherForecast
//
//  Created by ersun alkan on 16.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell
{
    
    @IBOutlet weak var dateLabel:UILabel!
    
    @IBOutlet weak var descriptionLabel:UILabel!
    
    @IBOutlet weak var celciusLabel:UILabel!
    
    func setModel(model:DailyForecastModel)
    {
        dateLabel.text = Date(timeIntervalSince1970: TimeInterval(model.dt)).toSimpleFormat()
        descriptionLabel.text = model.weather.first?.description
        celciusLabel.text = model.temp.dayTemperature.toCelsius()    
    }

   

}
