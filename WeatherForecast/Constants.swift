//
//  Constants.swift
//  WeatherForecast
//
//  Created by ersun alkan on 17.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit

enum NotificationConstants:String
{
    case CityListNeedsRefresh = "CityListNeedsRefresh"
    
    var notification : Notification.Name
    {
        return Notification.Name(rawValue: self.rawValue)
    }
}

enum Constants : String
{
    case CityList="CityList"
}
