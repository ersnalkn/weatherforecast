//
//  SavedCityModel.swift
//  WeatherForecast
//
//  Created by ersun alkan on 16.06.2019.
//  Copyright © 2019 N11.com. All rights reserved.
//

import UIKit
import Foundation

@objc class SavedCityModel:NSObject, NSCoding
{
    
    @objc var cityName:String
    var numbersOfDay:Int
    
    init(with name:String,day:Int=1)
    {
        self.cityName=name
        self.numbersOfDay=day
    }
    
    
    //MARK: NSCoding
    
    required init(coder aDecoder: NSCoder) {
        self.cityName = aDecoder.decodeObject(forKey: "cityName") as! String
        self.numbersOfDay = aDecoder.decodeInteger(forKey: "numbersOfDay")
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(cityName, forKey: "cityName")
        aCoder.encode(numbersOfDay, forKey: "numbersOfDay")
    }

}
