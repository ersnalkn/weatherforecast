//
//  Date+Extention.swift
//  OBilet
//
//  Created by ersun alkan on 11.06.2019.
//  Copyright © 2019 ersun alkan. All rights reserved.
//

import UIKit

extension Date {

    
    func toSimpleFormat(format: String = "EEEE dd MMM") -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = .short
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    

}

